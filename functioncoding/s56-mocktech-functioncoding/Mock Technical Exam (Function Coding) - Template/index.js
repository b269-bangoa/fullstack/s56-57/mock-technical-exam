function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if(letter.length === 1 && letter.match(/[a-z]/i)) {
        for (let i = 0; i < sentence.length; i++) 
        {
           if (sentence.charAt(i) == letter) 
             {
             result += 1;
             }
         }
         return result;   
    } else {
        return undefined;
    }   
};

//console.log(countLetter("l", "Hello, world"));

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let t = text.toLowerCase();

    for (let i = 0; i < t.length; i++) {
        if ( t.indexOf(t[i]) !== t.lastIndexOf(t[i]) ) {
            return false;
        } 
    }

    return true; 
};

//console.log(isIsogram("eanUIHswd")); for checking

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if(age < 13) {
        return undefined;
    } else if(age >= 13 && age <= 21) {
        let discountedPrice = parseFloat(price) * 0.8;
        return Number.parseFloat(discountedPrice).toFixed(2);
    } else if(age > 65){
        let discountedPrice = price * 0.8
        return Number.parseFloat(discountedPrice).toFixed(2);
    } else {
        return Number.parseFloat(price).toFixed(2);
    }
};

//console.log(typeof purchase(70, 335.45)); for checking

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    // The passed items array from the test are the following:

    /*items = [
        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    ]*/

    let hotCategories = [];

    let foundCategory = items.map(item => {
        if(item.stocks === 0) {
            hotCategories.push(item.category); 
        }
    })

    let uniqueHotCategories = hotCategories.filter((c, index) => {
        return hotCategories.indexOf(c) === index;
    });

    //console.log(uniqueHotCategories);
    return uniqueHotCategories;
}

//console.log(findHotCategories());   

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    // The passed values from the test are the following:
    
    let candidates = candidateB.concat(candidateA);

    let duplicateVoters = candidates.filter((voter, index) => {
        return candidates.indexOf(voter) !== index;
    });

    //console.log(duplicateVoters);
    return duplicateVoters;
};

/*let candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
let candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];*/

//console.log(findFlyingVoters(candidateA, candidateB));

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};